<html>
<body>
<h1>Servlet 03 (example)</h1>

<h3>Servler 1: Configuration</h3><br>
<a href="/servlet03/hello">Default</a>
<FORM ACTION="/servlet03/hello"
      METHOD="GET">
    Imie:
    <INPUT TYPE="TEXT" NAME="WHO">
    <INPUT TYPE="SUBMIT" NAME="Wyslij">
</FORM>
<hr>

<h3>Servler 2: Forwarding</h3>
<a href="/servlet03/redirects">Strona przekierowania</a><br>
<a href="/servlet03/createRedirects">Po stronie klienta</a><br>
<a href="/servlet03/servletRedirectServlet">Po stronie serwera</a><br>
<a href="/servlet03/filter">Filter</a>

</body>
</html>
